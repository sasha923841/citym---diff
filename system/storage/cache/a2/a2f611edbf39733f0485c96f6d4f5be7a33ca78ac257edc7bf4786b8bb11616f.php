<?php

/* so-emarket/template/common/footer.twig */
class __TwigTemplate_95018a3b5492fc813fffff9043772413ea419b30811345b052cbbc294bf93010 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        ob_start();
        // line 3
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "1")) {
            // line 4
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer1.twig"), "so-emarket/template/common/footer.twig", 4)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 5
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "2")) {
            // line 6
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer2.twig"), "so-emarket/template/common/footer.twig", 6)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 7
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "3")) {
            // line 8
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer3.twig"), "so-emarket/template/common/footer.twig", 8)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 9
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "4")) {
            // line 10
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer4.twig"), "so-emarket/template/common/footer.twig", 10)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 11
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "5")) {
            // line 12
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer5.twig"), "so-emarket/template/common/footer.twig", 12)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 13
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "6")) {
            // line 14
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer6.twig"), "so-emarket/template/common/footer.twig", 14)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 15
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "7")) {
            // line 16
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer7.twig"), "so-emarket/template/common/footer.twig", 16)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 17
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "8")) {
            // line 18
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer8.twig"), "so-emarket/template/common/footer.twig", 18)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 19
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "9")) {
            // line 20
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer9.twig"), "so-emarket/template/common/footer.twig", 20)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 21
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "10")) {
            // line 22
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer10.twig"), "so-emarket/template/common/footer.twig", 22)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 23
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "11")) {
            // line 24
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer11.twig"), "so-emarket/template/common/footer.twig", 24)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 25
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "12")) {
            // line 26
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer12.twig"), "so-emarket/template/common/footer.twig", 26)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 27
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "13")) {
            // line 28
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer13.twig"), "so-emarket/template/common/footer.twig", 28)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 29
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "14")) {
            // line 30
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer14.twig"), "so-emarket/template/common/footer.twig", 30)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 31
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "15")) {
            // line 32
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer15.twig"), "so-emarket/template/common/footer.twig", 32)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 33
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "16")) {
            // line 34
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer16.twig"), "so-emarket/template/common/footer.twig", 34)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 35
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "17")) {
            // line 36
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer17.twig"), "so-emarket/template/common/footer.twig", 36)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 37
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "18")) {
            // line 38
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer18.twig"), "so-emarket/template/common/footer.twig", 38)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 39
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "19")) {
            // line 40
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer19.twig"), "so-emarket/template/common/footer.twig", 40)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        } elseif (($this->getAttribute(        // line 41
(isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method") == "20")) {
            // line 42
            echo "\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/footer/footer20.twig"), "so-emarket/template/common/footer.twig", 42)->display(array_merge($context, array("typefooter" => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "typefooter"), "method"))));
        }
        // line 44
        echo "

\t
";
        // line 49
        echo "<!-- <div class=\"back-to-top\"><i class=\"fa fa-angle-up\"></i></div>-->
";
        // line 51
        echo "<div id=\"previewModal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\">
  <div class=\"modal-dialog modal-lg\" role=\"document\">
    <div class=\"modal-content\">
\t\t<div class=\"modal-header\">
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t<h4 class=\"modal-title\"> ";
        // line 56
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_alertaddtocart"), "method");
        echo "</h4>
\t\t</div>
\t\t<div class=\"modal-body\"></div>
    </div>
  </div>
</div>

";
        // line 63
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "social_sidebar"), "method")) {
            // line 64
            echo "\t";
            if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "social_sidebar"), "method") == 1)) {
                // line 65
                echo "\t\t";
                $context["social_sidebar"] = "socialwidgets-left";
                // line 66
                echo "\t";
            } else {
                // line 67
                echo "\t\t";
                $context["social_sidebar"] = "socialwidgets-right";
                // line 68
                echo "\t";
            }
            // line 69
            echo "\t<section class=\"social-widgets visible-lg ";
            echo (isset($context["social_sidebar"]) ? $context["social_sidebar"] : null);
            echo " \">
\t\t<ul class=\"items\">
\t\t\t";
            // line 71
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "social_fb_status"), "method")) {
                echo " 
\t\t\t<li class=\"item item-01 facebook\">
\t\t\t\t<a href=\"catalog/view/theme/";
                // line 73
                echo (isset($context["theme_directory"]) ? $context["theme_directory"] : null);
                echo "/template/social/facebook.php?account_fb=";
                echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "facebook"), "method");
                echo " \" class=\"tab-icon\"><span class=\"fa fa-facebook\"></span></a>
\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t<div class=\"title\"><h5>FACEBOOK</h5></div>
\t\t\t\t\t<div class=\"loading\">
\t\t\t\t\t\t<img class=\"lazyload\" data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"catalog/view/theme/";
                // line 77
                echo (isset($context["theme_directory"]) ? $context["theme_directory"] : null);
                echo "/images/ajax-loader.gif\" class=\"ajaxloader\" alt=\"loader\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</li>
\t\t\t";
            }
            // line 81
            echo " 

\t\t\t";
            // line 83
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "social_twitter_status"), "method")) {
                echo " 
\t\t\t<li class=\"item item-02 twitter\">
\t\t\t\t<a href=\"catalog/view/theme/";
                // line 85
                echo (isset($context["theme_directory"]) ? $context["theme_directory"] : null);
                echo "/template/social/twitter.php?account_twitter=";
                echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "twitter"), "method");
                echo " \" class=\"tab-icon\"><span class=\"fa fa-twitter\"></span></a>
\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t<div class=\"title\"><h5>TWITTER FEEDS</h5></div>
\t\t\t\t\t<div class=\"loading\">
\t\t\t\t\t\t<img class=\"lazyload\" data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"catalog/view/theme/";
                // line 89
                echo (isset($context["theme_directory"]) ? $context["theme_directory"] : null);
                echo "/images/ajax-loader.gif\" class=\"ajaxloader\" alt=\"loader\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</li>
\t\t\t";
            }
            // line 93
            echo " 

\t\t\t";
            // line 95
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "social_custom_status"), "method")) {
                echo " 
\t\t\t<li class=\"item item-03 youtube\">
\t\t\t\t<div class=\"tab-icon\"><span class=\"fa fa-youtube\"></span></div>
\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t<div class=\"loading\">
\t\t\t\t\t\t";
                // line 100
                if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "video_code"), "method")) {
                    // line 101
                    echo "\t\t\t\t\t\t\t\t";
                    echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "video_code"), "method")), "method");
                    echo "
\t\t\t\t\t\t";
                }
                // line 102
                echo " 
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</li>
\t\t\t";
            }
            // line 106
            echo " 
\t\t</ul>
\t</section>
\t
";
        }
        // line 110
        echo " 
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 112
        echo "</div>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "so-emarket/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  256 => 112,  252 => 110,  245 => 106,  238 => 102,  232 => 101,  230 => 100,  222 => 95,  218 => 93,  210 => 89,  201 => 85,  196 => 83,  192 => 81,  184 => 77,  175 => 73,  170 => 71,  164 => 69,  161 => 68,  158 => 67,  155 => 66,  152 => 65,  149 => 64,  147 => 63,  137 => 56,  130 => 51,  127 => 49,  122 => 44,  118 => 42,  116 => 41,  113 => 40,  111 => 39,  108 => 38,  106 => 37,  103 => 36,  101 => 35,  98 => 34,  96 => 33,  93 => 32,  91 => 31,  88 => 30,  86 => 29,  83 => 28,  81 => 27,  78 => 26,  76 => 25,  73 => 24,  71 => 23,  68 => 22,  66 => 21,  63 => 20,  61 => 19,  58 => 18,  56 => 17,  53 => 16,  51 => 15,  48 => 14,  46 => 13,  43 => 12,  41 => 11,  38 => 10,  36 => 9,  33 => 8,  31 => 7,  28 => 6,  26 => 5,  23 => 4,  21 => 3,  19 => 2,);
    }
}
/* {# =========== Show Header==============#}*/
/* {% spaceless %}*/
/* {% if soconfig.get_settings('typefooter') =='1'%}*/
/* 	{% include theme_directory~'/template/footer/footer1.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='2'%}*/
/* 	{% include theme_directory~'/template/footer/footer2.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='3'%}*/
/* 	{% include theme_directory~'/template/footer/footer3.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='4'%}*/
/* 	{% include theme_directory~'/template/footer/footer4.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='5'%}*/
/* 	{% include theme_directory~'/template/footer/footer5.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='6'%}*/
/* 	{% include theme_directory~'/template/footer/footer6.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='7'%}*/
/* 	{% include theme_directory~'/template/footer/footer7.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='8'%}*/
/* 	{% include theme_directory~'/template/footer/footer8.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='9'%}*/
/* 	{% include theme_directory~'/template/footer/footer9.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='10'%}*/
/* 	{% include theme_directory~'/template/footer/footer10.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='11'%}*/
/* 	{% include theme_directory~'/template/footer/footer11.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='12'%}*/
/* 	{% include theme_directory~'/template/footer/footer12.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='13'%}*/
/* 	{% include theme_directory~'/template/footer/footer13.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='14'%}*/
/* 	{% include theme_directory~'/template/footer/footer14.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='15'%}*/
/* 	{% include theme_directory~'/template/footer/footer15.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='16'%}*/
/* 	{% include theme_directory~'/template/footer/footer16.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='17'%}*/
/* 	{% include theme_directory~'/template/footer/footer17.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='18'%}*/
/* 	{% include theme_directory~'/template/footer/footer18.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='19'%}*/
/* 	{% include theme_directory~'/template/footer/footer19.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% elseif soconfig.get_settings('typefooter') =='20'%}*/
/* 	{% include theme_directory~'/template/footer/footer20.twig' with {typefooter: soconfig.get_settings('typefooter')} %}*/
/* {% endif %}*/
/* */
/* */
/* 	*/
/* {# =========== Show BackToTop==============#}*/
/* {# {% if soconfig.get_settings('backtop') %} #}*/
/* <!-- <div class="back-to-top"><i class="fa fa-angle-up"></i></div>-->*/
/* {# {% endif %} #}*/
/* <div id="previewModal" class="modal fade" tabindex="-1" role="dialog">*/
/*   <div class="modal-dialog modal-lg" role="document">*/
/*     <div class="modal-content">*/
/* 		<div class="modal-header">*/
/* 			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/* 			<h4 class="modal-title"> {{objlang.get('text_alertaddtocart') }}</h4>*/
/* 		</div>*/
/* 		<div class="modal-body"></div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* */
/* {% if soconfig.get_settings('social_sidebar')  %}*/
/* 	{% if soconfig.get_settings('social_sidebar') == 1 %}*/
/* 		{% set  social_sidebar = 'socialwidgets-left'%}*/
/* 	{% else %}*/
/* 		{% set  social_sidebar = 'socialwidgets-right'%}*/
/* 	{% endif %}*/
/* 	<section class="social-widgets visible-lg {{social_sidebar}} ">*/
/* 		<ul class="items">*/
/* 			{% if soconfig.get_settings('social_fb_status') %} */
/* 			<li class="item item-01 facebook">*/
/* 				<a href="catalog/view/theme/{{ theme_directory }}/template/social/facebook.php?account_fb={{soconfig.get_settings('facebook')}} " class="tab-icon"><span class="fa fa-facebook"></span></a>*/
/* 				<div class="tab-content">*/
/* 					<div class="title"><h5>FACEBOOK</h5></div>*/
/* 					<div class="loading">*/
/* 						<img class="lazyload" data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="catalog/view/theme/{{ theme_directory }}/images/ajax-loader.gif" class="ajaxloader" alt="loader">*/
/* 					</div>*/
/* 				</div>*/
/* 			</li>*/
/* 			{% endif %} */
/* */
/* 			{% if soconfig.get_settings('social_twitter_status') %} */
/* 			<li class="item item-02 twitter">*/
/* 				<a href="catalog/view/theme/{{ theme_directory }}/template/social/twitter.php?account_twitter={{ soconfig.get_settings('twitter')}} " class="tab-icon"><span class="fa fa-twitter"></span></a>*/
/* 				<div class="tab-content">*/
/* 					<div class="title"><h5>TWITTER FEEDS</h5></div>*/
/* 					<div class="loading">*/
/* 						<img class="lazyload" data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="catalog/view/theme/{{ theme_directory }}/images/ajax-loader.gif" class="ajaxloader" alt="loader">*/
/* 					</div>*/
/* 				</div>*/
/* 			</li>*/
/* 			{% endif %} */
/* */
/* 			{% if soconfig.get_settings('social_custom_status') %} */
/* 			<li class="item item-03 youtube">*/
/* 				<div class="tab-icon"><span class="fa fa-youtube"></span></div>*/
/* 				<div class="tab-content">*/
/* 					<div class="loading">*/
/* 						{% if soconfig.get_settings('video_code') %}*/
/* 								{{ soconfig.decode_entities( soconfig.get_settings('video_code') ) }}*/
/* 						{% endif %} */
/* 					</div>*/
/* 				</div>*/
/* 			</li>*/
/* 			{% endif %} */
/* 		</ul>*/
/* 	</section>*/
/* 	*/
/* {% endif %} */
/* {% endspaceless %}*/
/* </div>*/
/* </body>*/
/* </html>*/
